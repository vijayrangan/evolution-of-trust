import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class CooperatePlayerTest {
    @Test
    public void shouldAlwaysReturn1() throws Exception {
        CooperatePlayer player = new CooperatePlayer();

        assertEquals(1, player.move());
    }
}
