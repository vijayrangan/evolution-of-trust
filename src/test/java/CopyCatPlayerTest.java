import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;

public class CopyCatPlayerTest {
    @Test
    public void shouldReturn1WhenOpponentHasNotMadeAMove() throws Exception {
        CopyCatPlayer copyCatPlayer = new CopyCatPlayer();

        assertEquals(1, copyCatPlayer.move());
    }

    @Test
    public void shouldReturnOpponentsMoveWhenOpponentMakesAMove() throws Exception {
        int opponentMove = 0;

        CopyCatPlayer copyCatPlayer = new CopyCatPlayer();
        copyCatPlayer.setOpponentMove(opponentMove);

        assertEquals(0, copyCatPlayer.move());
    }
}
