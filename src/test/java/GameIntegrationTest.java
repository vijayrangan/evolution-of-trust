import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class GameIntegrationTest {
    @Test
    public void shouldReturnMinus1and3ForCopyCatAndCheatForOneRound() {
        Machine machine = new Machine();
        CopyCatPlayer copyCatPlayer = new CopyCatPlayer();
        CheatPlayer cheatPlayer = new CheatPlayer();

        Game game = new Game(machine, copyCatPlayer, cheatPlayer, 1);

        String result = game.play();

        assertEquals("-1, 3", result);
    }

    @Test
    public void shouldReturnMinus5and15ForCooperateAndCheat() {
        Machine machine = new Machine();
        CooperatePlayer cooperatePlayer = new CooperatePlayer();
        CheatPlayer cheatPlayer = new CheatPlayer();
        Game game = new Game(machine, cooperatePlayer, cheatPlayer, 5);

        String result = game.play();

        assertEquals("-5, 15", result);
    }
}
