import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class GrudgerPlayerTest {
    @Test
    public void shouldReturn1IfTheOpponentHasNotCheated() throws Exception {
        GrudgerPlayer grudgerPlayer = new GrudgerPlayer();

        assertEquals(1, grudgerPlayer.move());
    }

    @Test
    public void shouldReturnMinus1IfTheOpponentHasCheated() throws Exception {
        GrudgerPlayer grudgerPlayer = new GrudgerPlayer();

        grudgerPlayer.setHasOpponentCheated(true);

        assertEquals(-1, grudgerPlayer.move());
    }
}
