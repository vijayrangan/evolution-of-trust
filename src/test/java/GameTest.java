import org.junit.Test;

import java.util.ArrayList;

import static junit.framework.TestCase.assertEquals;
import static org.mockito.Mockito.*;

public class GameTest {
    @Test
    public void shouldReturnTheScoresOfBothPlayersWhenOneRoundIsComplete() throws Exception {
        // Arrange
        Machine machine = mock(Machine.class);
        ConsolePlayer playerOne = mock(ConsolePlayer.class);
        ConsolePlayer playerTwo = mock(ConsolePlayer.class);

        when(playerOne.move()).thenReturn(1);
        when(playerTwo.move()).thenReturn(1);

        ArrayList<Integer> score = new ArrayList<>(2);
        score.add(2);
        score.add(2);
        when(machine.calculateScore(1, 1)).thenReturn(score);

        // Act
        Game game = new Game(machine, playerOne, playerTwo, 2);

        String result = game.play();

        // Assert
        assertEquals("2, 2", result);
        verify(machine, times(1)).calculateScore(1, 1);
        verify(playerOne, times(1)).move();
        verify(playerTwo, times(1)).move();
    }

    @Test
    public void shouldTrackCumulativeScoreOfBothPlayers() throws Exception {
        Machine machine = mock(Machine.class);
        ConsolePlayer playerOne = mock(ConsolePlayer.class);
        ConsolePlayer playerTwo = mock(ConsolePlayer.class);

        when(playerOne.move()).thenReturn(1);
        when(playerTwo.move()).thenReturn(1);

        ArrayList<Integer> score = new ArrayList<>(2);
        score.add(2);
        score.add(2);
        when(machine.calculateScore(1, 1)).thenReturn(score);

        Game game = new Game(machine, playerOne, playerTwo, 1);
        game.play();

        assertEquals(2, game.scores()[0]);
        assertEquals(2, game.scores()[1]);
    }

    @Test
    public void shouldPlay2RoundsWhenInitializedWith2Rounds() throws Exception {
        Machine machine = mock(Machine.class);
        ConsolePlayer playerOne = mock(ConsolePlayer.class);
        ConsolePlayer playerTwo = mock(ConsolePlayer.class);

        when(playerOne.move()).thenReturn(1);
        when(playerTwo.move()).thenReturn(1);

        ArrayList<Integer> scores = new ArrayList<>();
        scores.add(2);
        scores.add(2);
        when(machine.calculateScore(1, 1)).thenReturn(scores);

        Game game = new Game(machine, playerOne, playerTwo, 2);
        game.play();

        verify(machine, times(2)).calculateScore(1, 1);
        verify(playerOne, times(2)).move();
        verify(playerTwo, times(2)).move();
    }

    @Test
    public void shouldReturnMinus1And3WhenCopyCatAndCheatPlay2OrMoreRounds() throws Exception {
        Machine machine = mock(Machine.class);
        CopyCatPlayer copyCatPlayer = mock(CopyCatPlayer.class);
        CheatPlayer cheatPlayer = mock(CheatPlayer.class);

        when(copyCatPlayer.move()).thenReturn(1);
        when(cheatPlayer.move()).thenReturn(0);

        ArrayList<Integer> score = new ArrayList<>();
        score.add(-1);
        score.add(3);
        when(machine.calculateScore(1, 0)).thenReturn(score);

        Game game = new Game(machine, copyCatPlayer, cheatPlayer, 3);
        String result = game.play();
    }
}
