import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class CheatPlayerTest {
    @Test
    public void shouldReturn0() throws Exception {
        CheatPlayer player = new CheatPlayer();

        assertEquals(0, player.move());
    }
}
