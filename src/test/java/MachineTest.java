import org.junit.Test;

import java.util.ArrayList;

import static junit.framework.TestCase.assertEquals;

public class MachineTest {
    @Test
    public void shouldReturnPositive3TokensWhenTheMoveIsCooperative() {
        Machine machine = new Machine();

        int playerOnceChoice = 1;
        int playerTwoChoice = 1;

        ArrayList<Integer> result = machine.calculateScore(playerOnceChoice, playerTwoChoice);

        assertEquals(2, (int) result.get(0));
        assertEquals(2, (int) result.get(1));
    }

    @Test
    public void shouldNotReturnAnyTokensForACheatMove() {
        Machine machine = new Machine();

        int playerOnceChoice = 0;
        int playerTwoChoice = 0;

        ArrayList<Integer> result = machine.calculateScore(playerOnceChoice, playerTwoChoice);

        assertEquals(0, (int) result.get(0));
        assertEquals(0, (int) result.get(1));
    }

    @Test
    public void shouldAwardPointsToPlayer2WhenPlayer2CheatsAndPlayer1Cooperates() {
        Machine machine = new Machine();

        int playerOnceChoice = 1;
        int playerTwoChoice = 0;

        ArrayList<Integer> result = machine.calculateScore(playerOnceChoice, playerTwoChoice);

        assertEquals(3, (int) result.get(1));
    }

    @Test
    public void shouldDeductPointsFromPlayer1WhenPlayer2CheatsAndPlayer1Cooperates() {
        Machine machine = new Machine();

        int playerOnceChoice = 1;
        int playerTwoChoice = 0;

        ArrayList<Integer> result = machine.calculateScore(playerOnceChoice, playerTwoChoice);

        assertEquals(-1, (int) result.get(0));
    }

    @Test
    public void shouldAwardPointsToPlayer1WhenPlayer2CooperatesAndPlayer1Cheats() {
        Machine machine = new Machine();

        int playerOnceChoice = 0;
        int playerTwoChoice = 1;

        ArrayList<Integer> result = machine.calculateScore(playerOnceChoice, playerTwoChoice);

        assertEquals(3, (int) result.get(0));
    }

    @Test
    public void shouldDeductPointsFromPlayer2WhenPlayer2CooperatesAndPlayer1Cheats() {
        Machine machine = new Machine();

        int playerOnceChoice = 0;
        int playerTwoChoice = 1;

        ArrayList<Integer> result = machine.calculateScore(playerOnceChoice, playerTwoChoice);

        assertEquals(-1, (int) result.get(1));
    }
}
