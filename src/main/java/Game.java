import java.util.ArrayList;

public class Game {
    Machine machine;
    Player playerOne;
    Player playerTwo;

    private int totalRounds;
    private int currentRound = 1;
    private int playerOneCumulativeScore = 0;
    private int playerTwoCumulativeScore = 0;

    public Game(Machine machine, Player playerOne, Player playerTwo, int rounds) {
        this.machine = machine;
        this.playerOne = playerOne;
        this.playerTwo = playerTwo;
        this.totalRounds = rounds;
    }

    public String play() {
        try {
            while (this.totalRounds > 0) {
                int playerOneChoice = this.playerOne.move();
                int playerTwoChoice = this.playerTwo.move();

                if (this.playerOne instanceof CopyCatPlayer) {
                    ((CopyCatPlayer) this.playerOne).setOpponentMove(playerTwoChoice);
                } else if (this.playerTwo instanceof CopyCatPlayer) {
                    ((CopyCatPlayer) this.playerTwo).setOpponentMove(playerOneChoice);
                }

                ArrayList<Integer> score = this.machine.calculateScore(playerOneChoice, playerTwoChoice);

                this.playerOneCumulativeScore += score.get(0);
                this.playerTwoCumulativeScore += score.get(1);

                this.currentRound += 1;
                this.totalRounds -= 1;
            }

            return String.format("%s, %s", this.scores()[0], this.scores()[1]);
        } catch (Exception e) {
            return e.getMessage();
        }
    }

    public int[] scores() {
        return new int[]{
                this.playerOneCumulativeScore,
                this.playerTwoCumulativeScore
        };
    }
}
