public class CopyCatPlayer implements Player {
    private int opponentMove;
    private boolean opponentHasMadeMove = false;

    @Override
    public int move() throws Exception {
        if (this.opponentHasMadeMove) {
            return this.opponentMove;
        }

        return 1;
    }

    public void setOpponentMove(int opponentMove) {
        this.opponentHasMadeMove = true;
        this.opponentMove = opponentMove;
    }
}
