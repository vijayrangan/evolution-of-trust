import java.util.ArrayList;

public class Machine {
    public ArrayList<Integer> calculateScore(int playerOneChoice, int playerTwoChoice) {
        ArrayList<Integer> result = new ArrayList<>();

        if (playerOneChoice == 1 && playerTwoChoice == 1) {
            result.add(2);
            result.add(2);
        }

        if (playerOneChoice == 0 && playerTwoChoice == 0) {
            result.add(0);
            result.add(0);
        }

        if (playerOneChoice == 1 && playerTwoChoice == 0) {
            result.add(-1);
            result.add(3);
        }

        if (playerOneChoice == 0 && playerTwoChoice == 1) {
            result.add(3);
            result.add(-1);
        }

        return result;
    }
}
