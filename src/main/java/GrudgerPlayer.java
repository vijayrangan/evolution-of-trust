public class GrudgerPlayer implements Player {
    private boolean hasOpponentCheated = false;
    @Override
    public int move() throws Exception {
        if (this.hasOpponentCheated) {
            return -1;
        }

        return 1;
    }

    public void setHasOpponentCheated(boolean hasCheated) {
        this.hasOpponentCheated = hasCheated;
    }
}
