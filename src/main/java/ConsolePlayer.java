import java.util.Scanner;

public class ConsolePlayer implements Player {

    private final Scanner console;
    public int score = 1;

    public ConsolePlayer(Scanner console) {
        this.console = console;
    }

    public int move() throws Exception {
        int move = this.console.nextInt();
        if (move == 1 || move == 0) {
            this.score--;
            return move;
        }

        throw new Exception("Invalid move. Please enter either 1 or 0");
    }
}
