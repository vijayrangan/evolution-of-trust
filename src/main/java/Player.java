public interface Player {
    public int move() throws Exception;
}
